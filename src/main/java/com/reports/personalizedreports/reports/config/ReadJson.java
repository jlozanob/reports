package com.reports.personalizedreports.reports.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import com.dcatech.commons.logs.logs.Filelogs;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;

public class ReadJson {

    public ReadJson(){}

    private ResourceLoader resourceLoader ;


    public  String RutaFile() throws  IOException{

       JSONParser parser = new JSONParser();
        Object obj;
        String vruta = "";
        //String path   = resourceLoader.getResource("classpath:reports/ruta.json").getURI().getPath();
        try {
            obj = parser.parse(new FileReader("c:/app/ruta.json"));
            JSONObject  jsonObject = (JSONObject) obj;
            vruta = (String) jsonObject.get("ruta");
        } catch (FileNotFoundException e) {
            //manejo de error
        } catch (IOException e) {
            Filelogs.registerFilelogs(e.getMessage(),ReadJson.class,3);
        } catch (ParseException e) {
            //manejo de error
        }

        return vruta;

    }


    public static String PatchFile() throws  IOException {
       String vruta = new ReadJson().RutaFile();

        return vruta;

    }

}
