package com.reports.personalizedreports.reports.controller;


import com.dcatech.commons.logs.logs.Filelogs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reports.personalizedreports.reports.service.impl.ReportsServiceImpl;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dcatech.commons.exception.exceptions.NullPointerException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("export")
public class ReportsController {

    @Autowired
    private ReportsServiceImpl reportsService;

    @GetMapping("/pdf")
    public ResponseEntity<byte[]> exportpdf(String reportname, String paramreports ) throws SQLException, JRException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> parames = new HashMap<String, Object>();
        try {
            parames = mapper.readValue(paramreports,Map.class);
        }catch (Exception e)
        {
            Filelogs.registerFilelogs(e.getMessage(),ReportsController.class,3);
            throw new NullPointerException(e.getMessage() + " ; Error creando el Map para el reporte" );
        }

        return reportsService.exportPdfFile(reportname,parames);
    }

    @GetMapping("/xlsx")
    public ResponseEntity<byte[]> exportexcel(String reportname,String paramreports) throws SQLException, JRException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> parames = new HashMap<String, Object>();
        try {
            parames = mapper.readValue(paramreports,Map.class);
        }catch (Exception e)
        {
            Filelogs.registerFilelogs(e.getMessage(),ReportsController.class,3);
            throw new NullPointerException(e.getMessage() + " ; Error creando el Map para el reporte" );
        }
        return reportsService.exportXlsxFile(reportname,parames);
    }

    @GetMapping("/docx")
    public ResponseEntity<byte[]> exportdoc(String reportname,String paramreports) throws SQLException, JRException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> parames = new HashMap<String, Object>();
        try {
            parames = mapper.readValue(paramreports,Map.class);
        }catch (Exception e)
        {
            Filelogs.registerFilelogs(e.getMessage(),ReportsController.class,3);
            throw new NullPointerException(e.getMessage() + " ; Error creando el Map para el reporte" );
        }
        return reportsService.exportDocxFile(reportname,parames);
    }
}
