package com.reports.personalizedreports.reports.service;

import net.sf.jasperreports.engine.JRException;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

public interface IReportsService {

    ResponseEntity<byte[]> exportPdfFile(String reportname,Map<String, Object> paramreports) throws SQLException, JRException, IOException;
    ResponseEntity<byte[]> exportXlsxFile(String reportname,Map<String, Object> paramreports) throws SQLException, JRException, IOException;
    ResponseEntity<byte[]> exportDocxFile(String reportname,Map<String, Object> paramreports) throws SQLException, JRException, IOException;
}
