package com.reports.personalizedreports.reports.repository.impl;


import com.reports.personalizedreports.reports.config.ReadJson;
import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;
import net.sf.jasperreports.engine.JRException;

import javax.sql.DataSource;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.dcatech.commons.exception.exceptions.FileNotFoundException;

@Repository
public class ReportsRepositoryJPA {




    @Autowired
    private DataSource dataSource;

    @Autowired
    private ResourceLoader resourceLoader;


    public JasperPrint generateReport(String reportname,Map<String, Object> paramreports) throws SQLException, IOException, JRException {
        String ruta =  ReadJson.PatchFile() +  reportname + ".jrxml";
        JasperPrint print ;
        try {
        JasperReport jasperReport = JasperCompileManager.compileReport(ruta);
        print = JasperFillManager.fillReport(jasperReport, paramreports, dataSource.getConnection());

        }
    catch (JRException e){
        throw  new FileNotFoundException( e.getMessage() + " ; " + FileNotFoundException.DESCRIPTION);
    }
        return print;
    }


}
