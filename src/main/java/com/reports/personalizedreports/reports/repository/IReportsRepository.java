package com.reports.personalizedreports.reports.repository;

import net.sf.jasperreports.engine.JRException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

public interface IReportsRepository {
    JasperPrint generateReport(String reportname,Map<String, Object> paramreports) throws SQLException, JRException, IOException;
}
