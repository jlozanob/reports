package com.reports.personalizedreports.reports.repository.impl;

import com.reports.personalizedreports.reports.config.ReadJson;
import com.reports.personalizedreports.reports.repository.IReportsRepository;
import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Transactional
@Repository
public class ReportsRepositoryimpl implements IReportsRepository  {

    @Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public JasperPrint generateReport(String reportname,Map<String, Object> paramreports) throws SQLException, JRException, IOException {
        Connection conn = jdbcTemplate.getDataSource().getConnection();

        String ruta = ReadJson.PatchFile() +  reportname + ".jrxml";
        JasperReport jasperReport = JasperCompileManager.compileReport(ruta);
        Map<String, Object> parameters = new HashMap<String, Object>();
        JasperPrint print = JasperFillManager.fillReport(jasperReport, paramreports, conn);
        return print;
    }

}
