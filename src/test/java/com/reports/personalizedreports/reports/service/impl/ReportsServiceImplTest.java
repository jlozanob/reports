package com.reports.personalizedreports.reports.service.impl;




import com.reports.personalizedreports.reports.repository.impl.ReportsRepositoryJPA;
import net.sf.jasperreports.engine.JRException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.dcatech.commons.exception.exceptions.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ReportsServiceImplTest {


    @Mock
    private DataSource dataSource;
    @Mock
    private Connection c;
    @Mock
    private PreparedStatement stmt;
    @Mock
    private ResultSet rs;


    @InjectMocks
    private ReportsRepositoryJPA reportsRepositoryJPA;


    @Before
    public void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        assertNotNull(dataSource);
       when(c.prepareStatement(any(String.class))).thenReturn(stmt);
        when(dataSource.getConnection()).thenReturn(c);

    }

    @Test
    public void exportPdfFileok() throws SQLException, IOException, JRException {

        Map<String, Object> parames = new HashMap<String, Object>();
        parames.put("s_sampleid","1812239734");
        String reporte = "Reporte_Samples";
        String nmreporte = reportsRepositoryJPA.generateReport(reporte,parames).getName();
        assertEquals(nmreporte,"ReporteMuestra");
        Assertions.assertThat("ReporteMuestra").isEqualTo(nmreporte);
    }

    @Test
    public void exportPdfFileFail() throws SQLException, IOException, JRException {

        Map<String, Object> parames = new HashMap<String, Object>();
        parames.put("s_sampleid","1812239734");
        String reporte = "Reporte_Samples";
        String nmreporte = reportsRepositoryJPA.generateReport(reporte,parames).getName();
        assertEquals(nmreporte,"ReporteGeneral");
        Assertions.assertThat("ReporteGeneral").isEqualTo(nmreporte);
    }


    @Test(expected = FileNotFoundException.class)
    public void exportPdfFileNoReport() throws SQLException, IOException, JRException {

        Map<String, Object> parames = new HashMap<String, Object>();
        parames.put("s_sampleid","1812239734");
        String reporte = "Reporte_Sample";
        when(reportsRepositoryJPA.generateReport(reporte,parames)).thenThrow(FileNotFoundException.class);

    }

}